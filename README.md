

1. npm install -g @angular/cli
2. ng new angular-electron
3. cd angular-electron
4. Change src/index.html.
	<base href="./">
	
5. Install electron
	npm install electron --save-dev
	
6. Now main.js in root directory
			
			
			const { app, BrowserWindow } = require('electron')
			const url = require("url");
			const path = require("path");

			let win;

			function createWindow () {
			  // Create the browser window.
			  win = new BrowserWindow({
				width: 600, 
				height: 600,
				backgroundColor: '#ffffff',
				icon: `file://${__dirname}/dist/assets/logo.png`
			  })


			  win.loadURL(`file://${__dirname}/dist/index.html`)

			  //// uncomment below to open the DevTools.
			  // win.webContents.openDevTools()

			  // Event when the window is closed.
			  win.on('closed', function () {
				win = null
			  })
			}

			// Create window on electron intialization
			app.on('ready', createWindow)

			// Quit when all windows are closed.
			app.on('window-all-closed', function () {

			  // On macOS specific close process
			  if (process.platform !== 'darwin') {
				app.quit()
			  }
			})

			app.on('activate', function () {
			  // macOS specific close process
			  if (win === null) {
				createWindow()
			  }
			})

7. Update package.json
				{
			  "name": "angular-electron",
			  "version": "0.0.0",
			  "license": "MIT",
			  "main": "main.js", // <-- update here
			  "scripts": {
				"ng": "ng",
				"start": "ng serve",
				"build": "ng build",
				"test": "ng test",
				"lint": "ng lint",
				"e2e": "ng e2e",
				"electron": "electron .", // <-- run electron 
				"electron-build": "ng build --prod && electron ." // <-- build app, then run electron 
			  },
			  // ...omitted
			}

8. Change angular.js 
	angular.json" set "projects>architect>build>options>outputPath" to "dist" only, deleting the extra project name folder
	
9. Run project
	npm run electron-build
	
	
